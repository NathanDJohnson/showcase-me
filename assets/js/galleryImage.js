import PropTypes from 'prop-types';

class GalleryImage extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <img
        className="showcaseme-image"
        data-id={this.props.image.id}
        src={this.props.image.src}
        alt={this.props.image.alt}
        onClick={this.props.onRemove}
      />
    );
  }
}

GalleryImage.PropTypes = {
  image: PropTypes.object.isRequired,
  onRemove: PropTypes.func.isRequired,
};

export default GalleryImage;
