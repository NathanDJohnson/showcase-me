import PropTypes from 'prop-types';
import Gallery from './gallery.js';
import Modal from './modal.js';

class Showcase extends React.Component {

  constructor(props) {
    super(props);
    this.addFromModal = this.addFromModal.bind(this);
    this.removeGallery = this.removeGallery.bind(this);
    this.galleryUpdate = this.galleryUpdate.bind(this);
    this.toggleModal = this.toggleModal.bind(this);
    this.state = {
      isOpen: false,
      items: this.props.items,
      galleryItems: this.props.items,
      modalItems: []
    };
  }

  componentDidUpdate() {
    this.props.onUpdate(this.state.items);
  }

  addFromModal( image ) {
    var clonedItems = JSON.parse(JSON.stringify(this.state.items));
    clonedItems.push(image);
    this.setState({
      items: clonedItems
    });
  }

  addGallery() {

  }

  removeGallery( event ) {
    var clonedItems = JSON.parse(JSON.stringify(this.state.items));
    for(var i = 0; i < clonedItems.length; i++ ) {
      if( Number(clonedItems[i].id) === Number(event.currentTarget.dataset.id) ) {
        clonedItems.splice(i,1);
      }
    }
    this.setState({items: clonedItems});
  }

  toggleModal() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  galleryUpdate() {
    this.props.onUpdate(this.state.items);
  }

  render() {
    return (
      <div className="showcase">
        <Gallery
          items={this.state.items}
          removeGallery={this.removeGallery}
          onUpdate={this.galleryUpdate}
        />
        <button onClick={this.toggleModal}>
          Open the modal
        </button>
        <Modal
          onClose={this.toggleModal}
          show={this.state.isOpen}
          onUpdate={this.addFromModal}
          items={this.state.items}>
        </Modal>
      </div>
    );
  }
}

Showcase.PropTypes = {
  onUpdate: PropTypes.func.isRequired,
};


export default Showcase;
