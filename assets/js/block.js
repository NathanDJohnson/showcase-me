import Showcase from './showcase.js';

const { registerBlockType } = wp.blocks;
const { RichText } = wp.editor;

registerBlockType( 'nathandjohnson/showcaseme', {
  title: 'ShowcaseMe',
  icon: 'format-image',
  category: 'layout',
  attributes: {
    title: {
      type: 'array',
      source: 'children',
      selector: 'h2'
    },
    images: {
      type: 'array',
      source: 'query',
      selector: 'img',
      query: {
        src: {
          type: 'string',
          source: 'attribute',
          attribute: 'src',
        },
        alt: {
          type: 'string',
          source: 'attribute',
          attribute: 'alt',
        },
        id: {
          type: 'string',
          source: 'attribute',
          attribute: 'data-id',
        }
      },
    }
  },
  edit( { attributes, className, setAttributes } ) {
    const { title, images } = attributes;
    const onChangeTitle = newTitle => {
      setAttributes( { title: newTitle } );
    };
    const onChangeImages = newImages => {
      setAttributes( { images: newImages } );
    };
    return (
      <div className={ className }>
        <RichText
          tagName="h2"
          value={ title }
          onChange={ onChangeTitle }
        />
        <Showcase
          items={ images }
          onUpdate={ onChangeImages }
        />
      </div>
    );
  },
  save( { attributes, className } ) {
    const { title, images } = attributes;
    return (
      <div className={ className }>
        <div className={ 'showcase' }>
          <h2>{ title }</h2>
          <div className="gallery">
            {images.map((image) => {
              return <img data-id={image.id} src={image.src} alt={image.alt} />
            })}
          </div>
        </div>
      </div>
    );
  }
});
