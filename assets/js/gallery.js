import PropTypes from 'prop-types';
import GalleryItem from './galleryItem';

class Gallery extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <ul className="gallery">
        {this.props.items.map( image => {
          return(
            <GalleryItem items={this.props.items} image={image} onRemove={this.props.removeGallery}/>
          );
        })}
      </ul>
    );
  }
}

Gallery.PropTypes = {
  items: PropTypes.array,
  removeGallery: PropTypes.func.isRequired,
};

export default Gallery;
