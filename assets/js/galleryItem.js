import PropTypes from 'prop-types';
import GalleryImage from './galleryImage';

class GalleryItem extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <li className="showcaseme-item">
        <GalleryImage image={this.props.image} onRemove={this.props.onRemove}/>
      </li>
    );
  }
}

GalleryItem.PropTypes = {
  image: PropTypes.object.isRequired,
  onRemove: PropTypes.func.isRequired,
};

export default GalleryItem;
