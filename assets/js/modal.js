import PropTypes from 'prop-types';

class Modal extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      items: [],
    };
  }

  componentDidMount() {
    var url = '/wp-json/wp/v2/item';
    fetch(url)
    .then(results => {
      return results.json();
    })
    .then(data => {
      let items = data.map((item) => {
        for( var i=0; i < this.props.items.length; i++ )
        {
          if( Number(item.id) === Number(this.props.items[i].id ) )
          {
            return null;
          }
        }
        return item;
      });
      var filtered = items.filter(function(el){
        return el != null;
      });
      this.setState({items: filtered});
    });
  }

  renderImage(image) {
    const listItemStyle = {
      margin: 10,
      height: 200,
      width: 300,
    };

    return (
      <li style={listItemStyle}>
        <img
          src={image.src}
          onClick={ ( event ) => {
            var clonedItems = JSON.parse(JSON.stringify(this.state.items));
            for(var i = 0; i < clonedItems.length; i++ ) {
              if( Number(clonedItems[i].id) === Number(event.currentTarget.dataset.id) ) {
                var id = clonedItems[i].id;
                clonedItems.splice(i,1);
                this.props.onUpdate(image);
              }
            }
            this.setState({items: clonedItems});
          }}
          data-id={image.id}
        />
      </li>
    );
  }

  render() {
    if(!this.props.show) {
      return null;
    }

    return (
      <div className="backdrop">
        <div className="modal">
          <ul className="list">
            {this.state.items.map(image => this.renderImage(image))}
          </ul>
          <div className="footer">
            <button className="close" onClick={this.props.onClose}>
              Close
            </button>
          </div>
        </div>
      </div>
    );
  }
}

Modal.PropTypes = {
  onClose: PropTypes.func.isRequired,
  show: PropTypes.bool,
  children: PropTypes.node
};

export default Modal;
