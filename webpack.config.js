var debug = process.env.NODE_ENV !== "production";
var output = debug ? 'showcaseme.js' : 'showcaseme.min.js';

module.exports = {
  entry: './assets/js/block.js',
  output: {
    path: __dirname,
    filename: 'assets/js/'+output,
  },
  module: {
    loaders: [
      {
        test: /.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
      },
    ],
  },
};
