<?php

/**
 * @package NathanDJohnson\ShowcaseMe
 */

declare( strict_types = 1 );
namespace NathanDJohnson\ShowcaseMe;

use NathanDJohnson\ShowcaseMe\Classes\Blocks\Showcase_Me;
use NathanDJohnson\ShowcaseMe\Classes\Post_Types\Item;
use NathanDJohnson\ShowcaseMe\Classes\Rest_Responses\Item_Image;

class Init
{

  protected static $_instance;

  protected $providers = [];

  public function init()
  {
    $this->register_providers();
  }

  protected function register_providers()
  {
    /* Remember to delete this */
    //$this->providers[] = new Galleries();

    /* Custom Post Types */
    $this->providers[] = new Item();

    /* Gutenberg Blocks */
    $this->providers[] = new Showcase_Me();

    /* REST API Responses */
    $this->providers[] = new Item_Image();

    foreach( $this->providers as $provider )
    {
      \add_action( $provider->get_hook(), function() use ( $provider ) {
        $provider->register();
      }, $provider->get_priority() );
    }
  }

  public static function instance() : self
  {
    if( ! isset( self::$_instance ) ) {
      $className = __CLASS__;
      new $className();
      self::$_instance = new $className();
    }
    return self::$_instance;
  }
}
