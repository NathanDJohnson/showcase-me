<?php

/**
 * @package NathanDJohnson\ShowcaseMe
 */

declare( strict_types = 1 );
namespace NathanDJohnson\ShowcaseMe\Interfaces;

interface iProvider
{
  public function register();
  public function get_hook() : string;
  public function get_priority() : int;
}
