<?php

/**
 * @package NathanDJohnson\ShowcaseMe
 */

declare( strict_types = 1 );
namespace NathanDJohnson\ShowcaseMe\Classes\Rest_Responses;

use NathanDJohnson\ShowcaseMe\Classes\Provider;

class Item_Image extends Provider
{

  protected $hook = 'rest_api_init';

  public function register()
  {
    \register_rest_field( 'item', 'src', [
      'get_callback' => function( $item ) {
        return \get_the_post_thumbnail_url( $item[ 'id' ] );
      },
    ]);

    \register_rest_field( 'item', 'alt', [
      'get_callback' => function( $item ) {
        return \get_the_excerpt( $item[ 'id' ] );
      },
    ]);
  }
}
