<?php

/**
 * @package NathanDJohnson\ShowcaseMe
 */

declare( strict_types = 1 );
namespace NathanDJohnson\ShowcaseMe\Classes\Post_Types;

use NathanDJohnson\ShowcaseMe\Classes\Provider;

class Item extends Provider
{

  protected $hook = 'init';

  public function register()
  {
    \register_post_type( 'item', [
      'label'               => \__( 'Item', 'showcaseme' ),
      'labels'              => [
        'name'                         => \_x( 'Items', 'Post Type General Name', 'showcaseme' ),
        'singular_name'                => \_x( 'Item', 'Post Type Singular Name', 'showcaseme' ),
        'add_new'                      => \__( 'Add New', 'showcaseme' ),
        'add_new_item'                 => \__( 'Add New Item', 'showcaseme' ),
        'edit_item'                    => \__( 'Edit Item', 'showcaseme' ),
        'new_item'                     => \__( 'New Item', 'showcaseme' ),
        'view_item'                    => \__( 'View Item', 'showcaseme' ),
        'view_items'                   => \__( 'View Items', 'showcaseme' ),
        'search_items'                 => \__( 'Search Item', 'showcaseme' ),
        'not_found'                    => \__( 'Not Found', 'showcaseme' ),
        'not_found_in_trash'           => \__( 'Not Found in Trash', 'showcaseme' ),
        'parent_item_colon'            => \__( '', 'showcaseme' ),
        'all_items'                    => \__( 'All Items', 'showcaseme' ),
        'archives'                     => \__( 'Item Archives', 'showcaseme' ),
        'attributes'                   => \__( 'Item Attributes', 'showcaseme' ),
        'insert_into_item'             => \__( 'Insert into Item', 'showcaseme' ),
        'uploaded_to_this_item'        => \__( 'Uploaded to this Item', 'showcaseme' ),
        'featured_image'               => \__( 'Featured Image', 'showcaseme' ),
        'set_featured_image'           => \__( 'Set Featured Image', 'showcaseme' ),
        'remove_featured_image'        => \__( 'Remove Featured Image', 'showcaseme' ),
        'use_featured_image'           => \__( 'Use Featured Image', 'showcaseme' ),
        'menu_name'                    => \__( 'Items', 'showcaseme' ),
        'filter_items_list'            => \__( 'Filter Items list', 'showcaseme' ),
        'items_list_navigation'        => \__( 'Items list navigation', 'showcaseme' ),
        'items_list'                   => \__( 'Items list', 'showcaseme' ),
        'item_published'               => \__( 'Item published', 'showcaseme' ),
        'item_published_privately'     => \__( 'Item published privately', 'showcaseme' ),
        'item_published_to_draft'      => \__( 'Item published to draft', 'showcaseme' ),
        'item_scheduled'               => \__( 'Item scheduled', 'showcaseme' ),
        'item_updated'                 => \__( 'Item updated', 'showcaseme' ),
      ],
      'description'         => \__( 'Items', 'showcaseme' ),
      'public'              => true,
      'hierarchical'        => false,
      'exclude_from_search' => false,
      'publicly_queryable'  => true,
      'show_ui'             => true,
      'show_ui_menu'        => true,
      'show_in_nav_menus'   => true,
      'show_in_admin_bar'   => true,
      'show_in_rest'        => true,
      'menu_position'       => 11,
      'menu_icon'           => 'dashicons-format-image',
      'capability_type'     => 'page',
      'supports'            => [ 'title', 'editor', 'thumbnail' ],
      'has_archive'         => true,
      'can_export'          => true,
    ] );
  }
}
