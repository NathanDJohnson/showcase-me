<?php

/**
 * @package NathanDJohnson\ShowcaseMe
 */

declare( strict_types = 1 );
namespace NathanDJohnson\ShowcaseMe\Classes\Post_Types;

use NathanDJohnson\ShowcaseMe\Classes\Provider;

class Galleries extends Provider
{

  protected $hook = 'init';

  public function register()
  {
    \register_post_type( 'galleries', [
      'label'               => \__( 'Gallery', 'showcaseme' ),
      'labels'              => [
        'name'                         => \_x( 'Galleries', 'Post Type General Name', 'showcaseme' ),
        'singular_name'                => \_x( 'Gallery', 'Post Type Singular Name', 'showcaseme' ),
        'add_new'                      => \__( 'Add New', 'showcaseme' ),
        'add_new_item'                 => \__( 'Add New Gallery', 'showcaseme' ),
        'edit_item'                    => \__( 'Edit Gallery', 'showcaseme' ),
        'new_item'                     => \__( 'New Gallery', 'showcaseme' ),
        'view_item'                    => \__( 'View Gallery', 'showcaseme' ),
        'view_items'                   => \__( 'View Galleries', 'showcaseme' ),
        'search_items'                 => \__( 'Search Gallery', 'showcaseme' ),
        'not_found'                    => \__( 'Not Found', 'showcaseme' ),
        'not_found_in_trash'           => \__( 'Not Found in Trash', 'showcaseme' ),
        'parent_item_colon'            => \__( '', 'showcaseme' ),
        'all_items'                    => \__( 'All Galleries', 'showcaseme' ),
        'archives'                     => \__( 'Gallery Archives', 'showcaseme' ),
        'attributes'                   => \__( 'Gallery Attributes', 'showcaseme' ),
        'insert_into_item'             => \__( 'Insert into Gallery', 'showcaseme' ),
        'uploaded_to_this_item'        => \__( 'Uploaded to this Gallery', 'showcaseme' ),
        'featured_image'               => \__( 'Featured Image', 'showcaseme' ),
        'set_featured_image'           => \__( 'Set Featured Image', 'showcaseme' ),
        'remove_featured_image'        => \__( 'Remove Featured Image', 'showcaseme' ),
        'use_featured_image'           => \__( 'Use Featured Image', 'showcaseme' ),
        'menu_name'                    => \__( 'Galleries', 'showcaseme' ),
        'filter_items_list'            => \__( 'Filter galleries list', 'showcaseme' ),
        'items_list_navigation'        => \__( 'Galleries list navigation', 'showcaseme' ),
        'items_list'                   => \__( 'Galleries list', 'showcaseme' ),
        'item_published'               => \__( 'Gallery published', 'showcaseme' ),
        'item_published_privately'     => \__( 'Gallery published privately', 'showcaseme' ),
        'item_published_to_draft'      => \__( 'Gallery published to draft', 'showcaseme' ),
        'item_scheduled'               => \__( 'Gallery scheduled', 'showcaseme' ),
        'item_updated'                 => \__( 'Gallery updated', 'showcaseme' ),
      ],
      'description'         => \__( 'Galleries', 'showcaseme' ),
      'public'              => true,
      'hierarchical'        => false,
      'exclude_from_search' => false,
      'publicly_queryable'  => true,
      'show_ui'             => true,
      'show_ui_menu'        => true,
      'show_in_nav_menus'   => true,
      'show_in_admin_bar'   => true,
      'show_in_rest'        => true,
      'menu_position'       => 11,
      'menu_icon'           => 'dashicons-format-image',
      'capability_type'     => 'page',
      'supports'            => [ 'title', 'editor', 'thumbnail' ],
      'has_archive'         => true,
      'can_export'          => true,
    ] );
  }
}
