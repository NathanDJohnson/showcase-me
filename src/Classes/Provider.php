<?php

/**
 * @package NathanDJohnson\ShowcaseMe
 */

declare( strict_types = 1 );
namespace NathanDJohnson\ShowcaseMe\Classes;

use NathanDJohnson\ShowcaseMe\Interfaces\iProvider;

abstract class Provider implements iProvider
{
  protected $hook = '';
  protected $priority = 10;

  public function get_hook() : string
  {
    return $this->hook;
  }

  public function get_priority() : int
  {
    return $this->priority;
  }
}
