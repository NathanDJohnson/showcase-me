<?php

/**
 * @package NathanDJohnson\ShowcaseMe
 */

declare( strict_types = 1 );
namespace NathanDJohnson\ShowcaseMe\Classes\Blocks;

use NathanDJohnson\ShowcaseMe\Classes\Provider;

class Showcase_me extends Provider
{

  protected $hook = 'enqueue_block_editor_assets';

  public function register()
  {
    \wp_enqueue_script(
      'showcase-me',
      \plugins_url( '/showcaseme/assets/js/showcaseme.js' ),
      [ 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor' ]
    );

    \wp_enqueue_style(
      'showcase-me',
      \plugins_url( '/showcaseme/assets/css/showcaseme-editor.css' )
    );
  }
}
