<?php
/*
 * Plugin Name: ShowcaseMe
 * Description: WordPress plugin to add project portfolios. Requires PHP 7.2+ and WordPress 5.0.0+.
 * Author: Nathan Johnson
 * Version: 1.0.0
 * Author URI: https://gitlab.com/NathanDJohnson
 */

/**
 * @package NathanDJohnson\ShowcaseMe
 */

declare( strict_types = 1 );
namespace NathanDJohnson\ShowcaseMe;

use NathanDJohnson\ShowcaseMe\Init;

define( 'SHOWCASEME_BASE_PATH', \plugin_dir_path( __FILE__ ) );
require_once SHOWCASEME_BASE_PATH . 'vendor/autoload.php';

\add_action( 'plugins_loaded', function() {
  Init::instance()->init();
} );
